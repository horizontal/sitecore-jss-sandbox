module.exports = {
  plugins: ['commitlint-plugin-jira-rules'],
  extends: ['jira'],
  rules: {
    'jira-task-id-project-key': [2, 'always', ['HDJ', 'HOR']],
    'header-full-stop': [2, 'always', '.'],
  }
}