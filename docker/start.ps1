﻿Import-Module -Name (Join-Path $PSScriptRoot ".\tools\util")

#----------------------------------------------------------
## clean up
#----------------------------------------------------------

docker system prune -f

#----------------------------------------------------------
## load variables
#----------------------------------------------------------

$url = Get-EnvVar -Key CM_HOST

#----------------------------------------------------------
## check license is present
#----------------------------------------------------------

$licensePath = Get-EnvVar -Key LICENSE_PATH
if (-not (Test-Path (Join-Path $licensePath "license.xml"))) {
    Write-Host "License file not present in folder." -ForegroundColor Red
    Break
}

#----------------------------------------------------------
## check traefik ssl certs present
#----------------------------------------------------------

if (-not (Test-Path .\traefik\certs\cert.pem)) {
    .\tools\mkcert.ps1 -FullHostName ($url -replace "^.+?(\.)", "")
}

[System.Environment]::SetEnvironmentVariable('NODE_EXTRA_CA_CERTS', "$env:LOCALAPPDATA\mkcert\rootCA.pem" , [System.EnvironmentVariableTarget]::User)

Write-Host "Cert Path: $env:LOCALAPPDATA\mkcert\rootCA.pem"
Write-Host "Env:NODE_EXTRA_CA_CERTS: $env:NODE_EXTRA_CA_CERTS"
#----------------------------------------------------------
## check if user override env file exists
#----------------------------------------------------------

Read-UserEnvFile

#----------------------------------------------------------
## build docker, should be quick if everything is up to date
#----------------------------------------------------------

docker-compose build

#----------------------------------------------------------
## start docker
#----------------------------------------------------------

docker-compose up -d

Wait-SiteResponsive
Write-Host "`n`nDone... opening https://$($url)" -ForegroundColor DarkGray
start "https://$url"
