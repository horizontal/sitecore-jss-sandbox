using System.Linq;
using Sandbox.Feature.Jss.ViewEngine.Http;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Sitecore.JavaScriptServices.ViewEngine.Http;

namespace Sandbox.Feature.Jss.DI
{
    [UsedImplicitly]
    public class RegisterContainer : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            // We need to remove the existing mapping before adding our own
            serviceCollection.Where(x => x.ServiceType == typeof(IHttpClientFactory))
                .ToList()
                .ForEach(x => serviceCollection.Remove(x));

            serviceCollection.AddTransient<IHttpClientFactory, LocalTunnelSupportedHttpClientFactory>();
        }
    }
}
