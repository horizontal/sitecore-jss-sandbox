using Sitecore.JavaScriptServices.ViewEngine.Http;

namespace Sandbox.Feature.Jss.ViewEngine.Http
{
    public class LocalTunnelSupportedHttpClientFactory : IHttpClientFactory
    {
        private readonly HttpClientFactory _baseImplementation;

        public LocalTunnelSupportedHttpClientFactory()
        {
            _baseImplementation = new HttpClientFactory();
        }
        public IHttpClient Create(HttpRenderEngineOptions options)
        {
            var httpClient = _baseImplementation.Create(options);
            if (httpClient is TimeoutCapableWebClient capableWebClient)
            {
                // Pass parameter to bypass local tunnel splash screen
                capableWebClient.Headers["Bypass-Tunnel-Reminder"] = "this-value-can-be-anything";
                return capableWebClient;
            }
            return httpClient;
        }
    }
}
