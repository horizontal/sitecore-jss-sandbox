using Sandbox.Foundation.DI.Extensions;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Sandbox.Foundation.DI
{
    [UsedImplicitly]
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddMvcControllers(
                "Sandbox.Feature.*");

            serviceCollection.AddClassesWithServiceConfigObjectAttribute(
                "Sandbox.Foundation.*");
            serviceCollection.AddClassesWithServiceConfigObjectAttribute(
                "Sandbox.Feature.*");
            serviceCollection.AddClassesWithServiceConfigObjectAttribute(
                "Sandbox.Project.*");
        }
    }
}
