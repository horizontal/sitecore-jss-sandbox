using Microsoft.Extensions.DependencyInjection;
using Sitecore.Mvc.Controllers;
using System;
using System.Linq;
using System.Reflection;
using Sandbox.Foundation.Core.Methods;
using Sandbox.Foundation.DI.Attributes;
using Sitecore.Diagnostics;

namespace Sandbox.Foundation.DI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMvcControllers(this IServiceCollection serviceCollection, params string[] assemblyFilters)
        {
            var assemblies = GetAssemblies.GetByFilter(assemblyFilters);

            AddMvcControllers(serviceCollection, assemblies);
        }

        public static void AddMvcControllers(this IServiceCollection serviceCollection, params Assembly[] assemblies)
        {
            var controllers = GetTypes.GetTypesImplementing<SitecoreController>(assemblies)
                .Where(controller => controller.Name.EndsWith("Controller", StringComparison.Ordinal));

            foreach (var controller in controllers)
            {
                serviceCollection.AddTransient(controller);
            }
        }

        public static void AddClassesWithServiceConfigObjectAttribute(this IServiceCollection serviceCollection, params string[] assemblyFilters)
        {
            var assemblies = GetAssemblies.GetByFilter(assemblyFilters);
            
            //throw new InvalidOperationException(string.Join(Environment.NewLine, assemblies.Select(x => x.FullName)));
            
            AddClassesWithServiceConfigObjectAttribute(serviceCollection, assemblies);
        }

        public static void AddClassesWithServiceConfigObjectAttribute(this IServiceCollection serviceCollection, params Assembly[] assemblies)
        {
            var typesWithAttributes = assemblies
                .Where(assembly => !assembly.IsDynamic)
                .SelectMany(x => x.GetExportedTypes())
                .Where(type => !type.IsAbstract && !type.IsGenericTypeDefinition)
                .Select(type => new
                {
                    type.GetCustomAttribute<ServiceImplementationAttribute>()?.ServiceType,
                    ImplementationType = type,
                    type.GetCustomAttribute<ServiceImplementationAttribute>()?.Lifetime
                })
                .Where(t => t.Lifetime != null);

            foreach (var type in typesWithAttributes)
            {
                AddConfigObject(serviceCollection, type.ServiceType ?? type.ImplementationType, type.ImplementationType, type.Lifetime.Value);
                //Log.Info($"Registering Implementation: {type.ImplementationType}, Service: {type.ServiceType}", typeof(ServiceCollectionExtensions));
            }

        }

        private static void AddConfigObject(IServiceCollection serviceCollection, Type serviceType, Type implementationType, ServiceLifetime lifetime)
        {
            if (serviceCollection == null || serviceType == null)
            {
                return;
            }

            switch (lifetime)
            {
                case ServiceLifetime.Singleton:
                    serviceCollection.AddSingleton(serviceType, implementationType);
                    break;
                case ServiceLifetime.Scoped:
                    serviceCollection.AddScoped(serviceType, implementationType);
                    break;
                case ServiceLifetime.Transient:
                    serviceCollection.AddTransient(serviceType, implementationType);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(lifetime), lifetime, null);
            }
        }
    }
}
