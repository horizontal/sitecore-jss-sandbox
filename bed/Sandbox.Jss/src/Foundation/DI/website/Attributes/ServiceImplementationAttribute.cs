using System;
using Microsoft.Extensions.DependencyInjection;

namespace Sandbox.Foundation.DI.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ServiceImplementationAttribute : Attribute
    {
        public ServiceImplementationAttribute(Type serviceType)
        {
            ServiceType = serviceType;
        }
        public ServiceLifetime Lifetime { get; set; } = ServiceLifetime.Scoped;

        public Type ServiceType { get; }
    }
}
