// ReSharper Disable all
using global::Sitecore.Data;
using global::Sitecore.Data.Fields;
using global::Sitecore.Data.Items;
using global::System.CodeDom.Compiler;

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseCtaItem
	{
		LinkField CtaField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseCta : CustomItem, IBaseCtaItem
	{
		public BaseCta(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Cta";
		public static ID ItemTemplateId => new ID("{4814A314-8FCE-4836-BE40-C1CC1D5CE687}");
		
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public static implicit operator BaseCta(Item item) => item != null ? new BaseCta(item) : null;
		public static implicit operator Item(BaseCta customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Filter
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseFilterableItem
	{
		MultilistField AuthorFilterField { get; }
		MultilistField UserFilterField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseFilterable : CustomItem, IBaseFilterableItem
	{
		public BaseFilterable(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Filterable";
		public static ID ItemTemplateId => new ID("{2C0E7DCC-33F7-43DA-A885-F91245461FDF}");
		
		public MultilistField AuthorFilterField => new MultilistField(InnerItem.Fields[FieldConstants.AuthorFilter.Id]);
		public MultilistField UserFilterField => new MultilistField(InnerItem.Fields[FieldConstants.UserFilter.Id]);
		public static implicit operator BaseFilterable(Item item) => item != null ? new BaseFilterable(item) : null;
		public static implicit operator Item(BaseFilterable customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct AuthorFilter
            {
		        public const string FieldName = "authorFilter";
		        public static readonly ID Id = new ID("{BE7A1B84-D85F-4D7C-8A8C-687ADC063151}");
            }
            public struct UserFilter
            {
		        public const string FieldName = "userFilter";
		        public static readonly ID Id = new ID("{807BEA77-C3AD-44DC-8847-8A4B07F12487}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseHeadingItem
	{
		ReferenceField HeadingLevelField { get; }
		CheckboxField HeadingScreenReaderOnlyField { get; }
		TextField HeadingTextField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseHeading : CustomItem, IBaseHeadingItem
	{
		public BaseHeading(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Heading";
		public static ID ItemTemplateId => new ID("{AC098F0C-4384-449C-8771-7652AEFA2EA6}");
		
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator BaseHeading(Item item) => item != null ? new BaseHeading(item) : null;
		public static implicit operator Item(BaseHeading customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseHeadingStylesItem
	{
		CheckboxField HeadingBrandingField { get; }
		ReferenceField HeadingSizeField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseHeadingStyles : CustomItem, IBaseHeadingStylesItem
	{
		public BaseHeadingStyles(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Heading Styles";
		public static ID ItemTemplateId => new ID("{5EE59F57-5D9B-4F20-97E3-0A4878053A1F}");
		
		public CheckboxField HeadingBrandingField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingBranding.Id]);
		public ReferenceField HeadingSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingSize.Id]);
		public static implicit operator BaseHeadingStyles(Item item) => item != null ? new BaseHeadingStyles(item) : null;
		public static implicit operator Item(BaseHeadingStyles customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingBranding
            {
		        public const string FieldName = "headingBranding";
		        public static readonly ID Id = new ID("{C6918A4E-89EC-449D-8F4D-9DC689D974AC}");
            }
            public struct HeadingSize
            {
		        public const string FieldName = "headingSize";
		        public static readonly ID Id = new ID("{7E5C33BC-FC23-4A36-8340-F4C53C469F7B}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseHeadingWithCtaItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseCtaItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseHeadingWithCta : CustomItem, IBaseHeadingWithCtaItem
	{
		public BaseHeadingWithCta(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Heading With Cta";
		public static ID ItemTemplateId => new ID("{3AA6AEE6-6E43-4C7C-8863-00EB69FD03FC}");
		
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public static implicit operator BaseHeadingWithCta(Item item) => item != null ? new BaseHeadingWithCta(item) : null;
		public static implicit operator Item(BaseHeadingWithCta customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseHeadingWithDescriptionItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingItem
	{
		TextField DescriptionField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseHeadingWithDescription : CustomItem, IBaseHeadingWithDescriptionItem
	{
		public BaseHeadingWithDescription(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Heading With Description";
		public static ID ItemTemplateId => new ID("{454CBC65-5A69-404A-8479-8DFE3DB50F62}");
		
		public TextField DescriptionField => new TextField(InnerItem.Fields[FieldConstants.Description.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator BaseHeadingWithDescription(Item item) => item != null ? new BaseHeadingWithDescription(item) : null;
		public static implicit operator Item(BaseHeadingWithDescription customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Description
            {
		        public const string FieldName = "description";
		        public static readonly ID Id = new ID("{0F66F48B-00DD-4FB8-866F-433B08271884}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseHeadingWithDescriptionAndCtaItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingWithDescriptionItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseCtaItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseHeadingWithDescriptionAndCta : CustomItem, IBaseHeadingWithDescriptionAndCtaItem
	{
		public BaseHeadingWithDescriptionAndCta(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Heading With Description And Cta";
		public static ID ItemTemplateId => new ID("{5937325F-244F-48B5-87ED-3D9AA287BF31}");
		
		public TextField DescriptionField => new TextField(InnerItem.Fields[FieldConstants.Description.Id]);
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator BaseHeadingWithDescriptionAndCta(Item item) => item != null ? new BaseHeadingWithDescriptionAndCta(item) : null;
		public static implicit operator Item(BaseHeadingWithDescriptionAndCta customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Description
            {
		        public const string FieldName = "description";
		        public static readonly ID Id = new ID("{0F66F48B-00DD-4FB8-866F-433B08271884}");
            }
            public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseStyleableHeadingItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingStylesItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseStyleableHeading : CustomItem, IBaseStyleableHeadingItem
	{
		public BaseStyleableHeading(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Style-able Heading";
		public static ID ItemTemplateId => new ID("{DDCE3423-6A30-4393-964B-F616086E5593}");
		
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public CheckboxField HeadingBrandingField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingBranding.Id]);
		public ReferenceField HeadingSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingSize.Id]);
		public static implicit operator BaseStyleableHeading(Item item) => item != null ? new BaseStyleableHeading(item) : null;
		public static implicit operator Item(BaseStyleableHeading customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            public struct HeadingBranding
            {
		        public const string FieldName = "headingBranding";
		        public static readonly ID Id = new ID("{C6918A4E-89EC-449D-8F4D-9DC689D974AC}");
            }
            public struct HeadingSize
            {
		        public const string FieldName = "headingSize";
		        public static readonly ID Id = new ID("{7E5C33BC-FC23-4A36-8340-F4C53C469F7B}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseStyleableHeadingWithCtaItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseStyleableHeadingItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseCtaItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseStyleableHeadingWithCta : CustomItem, IBaseStyleableHeadingWithCtaItem
	{
		public BaseStyleableHeadingWithCta(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Style-able Heading With Cta";
		public static ID ItemTemplateId => new ID("{360D9460-AF6E-4E71-9CF6-000319A1BB26}");
		
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public CheckboxField HeadingBrandingField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingBranding.Id]);
		public ReferenceField HeadingSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingSize.Id]);
		public static implicit operator BaseStyleableHeadingWithCta(Item item) => item != null ? new BaseStyleableHeadingWithCta(item) : null;
		public static implicit operator Item(BaseStyleableHeadingWithCta customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            public struct HeadingBranding
            {
		        public const string FieldName = "headingBranding";
		        public static readonly ID Id = new ID("{C6918A4E-89EC-449D-8F4D-9DC689D974AC}");
            }
            public struct HeadingSize
            {
		        public const string FieldName = "headingSize";
		        public static readonly ID Id = new ID("{7E5C33BC-FC23-4A36-8340-F4C53C469F7B}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseStyleableHeadingWithDescriptionItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingWithDescriptionItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingStylesItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseStyleableHeadingWithDescription : CustomItem, IBaseStyleableHeadingWithDescriptionItem
	{
		public BaseStyleableHeadingWithDescription(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Base Style-able Heading With Description";
		public static ID ItemTemplateId => new ID("{FA9FCE0C-CF2F-4DBC-89B0-38C6CD94EC89}");
		
		public TextField DescriptionField => new TextField(InnerItem.Fields[FieldConstants.Description.Id]);
		public CheckboxField HeadingBrandingField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingBranding.Id]);
		public ReferenceField HeadingSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingSize.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator BaseStyleableHeadingWithDescription(Item item) => item != null ? new BaseStyleableHeadingWithDescription(item) : null;
		public static implicit operator Item(BaseStyleableHeadingWithDescription customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Description
            {
		        public const string FieldName = "description";
		        public static readonly ID Id = new ID("{0F66F48B-00DD-4FB8-866F-433B08271884}");
            }
            public struct HeadingBranding
            {
		        public const string FieldName = "headingBranding";
		        public static readonly ID Id = new ID("{C6918A4E-89EC-449D-8F4D-9DC689D974AC}");
            }
            public struct HeadingSize
            {
		        public const string FieldName = "headingSize";
		        public static readonly ID Id = new ID("{7E5C33BC-FC23-4A36-8340-F4C53C469F7B}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseRenderingParametersItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseRenderingParameters : CustomItem, IBaseRenderingParametersItem
	{
		public BaseRenderingParameters(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "BaseRenderingParameters";
		public static ID ItemTemplateId => new ID("{D24BB6F7-6088-4CBF-A4CC-5746E8AE78A9}");
		
		public static implicit operator BaseRenderingParameters(Item item) => item != null ? new BaseRenderingParameters(item) : null;
		public static implicit operator Item(BaseRenderingParameters customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.Sxa.Multisite
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface ISandboxHeadlessSettingsItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class SandboxHeadlessSettings : CustomItem, ISandboxHeadlessSettingsItem
	{
		public SandboxHeadlessSettings(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Sandbox Headless Settings";
		public static ID ItemTemplateId => new ID("{EF4B4296-28E8-4577-AAE9-BE156C789BF4}");
		
		public static implicit operator SandboxHeadlessSettings(Item item) => item != null ? new SandboxHeadlessSettings(item) : null;
		public static implicit operator Item(SandboxHeadlessSettings customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseComponentItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow.IBaseWorkflowItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseComponent : CustomItem, IBaseComponentItem
	{
		public BaseComponent(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Component";
		public static ID ItemTemplateId => new ID("{0E0A84C4-B548-419A-A21F-6D21AA2264C7}");
		
		public static implicit operator BaseComponent(Item item) => item != null ? new BaseComponent(item) : null;
		public static implicit operator Item(BaseComponent customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Data
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseDataItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow.IBaseWorkflowItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseData : CustomItem, IBaseDataItem
	{
		public BaseData(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Data";
		public static ID ItemTemplateId => new ID("{A25B8BF4-44AD-44CD-83B3-ADECB61279C6}");
		
		public static implicit operator BaseData(Item item) => item != null ? new BaseData(item) : null;
		public static implicit operator Item(BaseData customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBasePageItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes.IBaseRouteItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes.IBasePageInfoItem
	{
		CheckboxField ExcludeFromNavigationField { get; }
		DateField LastUpdatedField { get; }
		TextField MetaDescriptionField { get; }
		ImageField MetaImageField { get; }
		ReferenceField PageThemeField { get; }
		CheckboxField SuppressFloatingCtaField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BasePage : CustomItem, IBasePageItem
	{
		public BasePage(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Page";
		public static ID ItemTemplateId => new ID("{D61CD849-3F62-422E-A72D-2A7E22466EE6}");
		
		public CheckboxField ExcludeFromNavigationField => new CheckboxField(InnerItem.Fields[FieldConstants.ExcludeFromNavigation.Id]);
		public DateField LastUpdatedField => new DateField(InnerItem.Fields[FieldConstants.LastUpdated.Id]);
		public TextField MetaDescriptionField => new TextField(InnerItem.Fields[FieldConstants.MetaDescription.Id]);
		public ImageField MetaImageField => new ImageField(InnerItem.Fields[FieldConstants.MetaImage.Id]);
		public ReferenceField PageThemeField => new ReferenceField(InnerItem.Fields[FieldConstants.PageTheme.Id]);
		public CheckboxField SuppressFloatingCtaField => new CheckboxField(InnerItem.Fields[FieldConstants.SuppressFloatingCta.Id]);
		public TextField PageDescriptionField => new TextField(InnerItem.Fields[FieldConstants.PageDescription.Id]);
		public TextField PageTitleField => new TextField(InnerItem.Fields[FieldConstants.PageTitle.Id]);
		public static implicit operator BasePage(Item item) => item != null ? new BasePage(item) : null;
		public static implicit operator Item(BasePage customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct ExcludeFromNavigation
            {
		        public const string FieldName = "excludeFromNavigation";
		        public static readonly ID Id = new ID("{22A4D090-D55C-4DCE-9BB9-7D9ED3D78519}");
            }
            public struct LastUpdated
            {
		        public const string FieldName = "lastUpdated";
		        public static readonly ID Id = new ID("{95026A67-37B4-4D88-88E3-27BA24F84A3E}");
            }
            public struct MetaDescription
            {
		        public const string FieldName = "metaDescription";
		        public static readonly ID Id = new ID("{5E6D77BC-9211-54A4-B2F1-8F69285FC51C}");
            }
            public struct MetaImage
            {
		        public const string FieldName = "metaImage";
		        public static readonly ID Id = new ID("{E73A482A-EE8E-432F-963A-65DAB0617C82}");
            }
            public struct PageTheme
            {
		        public const string FieldName = "pageTheme";
		        public static readonly ID Id = new ID("{D0E54558-7232-5B88-86C8-F615641E8B53}");
            }
            public struct SuppressFloatingCta
            {
		        public const string FieldName = "suppressFloatingCta";
		        public static readonly ID Id = new ID("{A4439BBB-8C0E-4B01-87EE-F3179835CB89}");
            }
            public struct PageDescription
            {
		        public const string FieldName = "pageDescription";
		        public static readonly ID Id = new ID("{4FE8C36B-0728-4973-9207-F4DCE5441F15}");
            }
            public struct PageTitle
            {
		        public const string FieldName = "pageTitle";
		        public static readonly ID Id = new ID("{EDC77533-31EC-5C72-A307-09CD06E8D153}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBasePageInfoItem
	{
		TextField PageDescriptionField { get; }
		TextField PageTitleField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BasePageInfo : CustomItem, IBasePageInfoItem
	{
		public BasePageInfo(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Page Info";
		public static ID ItemTemplateId => new ID("{B0E9BBBA-0B88-446A-A941-D23D775B5D7C}");
		
		public TextField PageDescriptionField => new TextField(InnerItem.Fields[FieldConstants.PageDescription.Id]);
		public TextField PageTitleField => new TextField(InnerItem.Fields[FieldConstants.PageTitle.Id]);
		public static implicit operator BasePageInfo(Item item) => item != null ? new BasePageInfo(item) : null;
		public static implicit operator Item(BasePageInfo customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct PageDescription
            {
		        public const string FieldName = "pageDescription";
		        public static readonly ID Id = new ID("{4FE8C36B-0728-4973-9207-F4DCE5441F15}");
            }
            public struct PageTitle
            {
		        public const string FieldName = "pageTitle";
		        public static readonly ID Id = new ID("{EDC77533-31EC-5C72-A307-09CD06E8D153}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseRouteItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow.IBaseWorkflowItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseRoute : CustomItem, IBaseRouteItem
	{
		public BaseRoute(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Route";
		public static ID ItemTemplateId => new ID("{2B03BA30-ACDA-4955-B331-60ECDFF30490}");
		
		public static implicit operator BaseRoute(Item item) => item != null ? new BaseRoute(item) : null;
		public static implicit operator Item(BaseRoute customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IBaseWorkflowItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class BaseWorkflow : CustomItem, IBaseWorkflowItem
	{
		public BaseWorkflow(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Base Workflow";
		public static ID ItemTemplateId => new ID("{D96AF3BA-23EB-4AFB-B3B5-E44EA77C30F7}");
		
		public static implicit operator BaseWorkflow(Item item) => item != null ? new BaseWorkflow(item) : null;
		public static implicit operator Item(BaseWorkflow customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IWorkflowIncludeChildrenFlagItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class WorkflowIncludeChildrenFlag : CustomItem, IWorkflowIncludeChildrenFlagItem
	{
		public WorkflowIncludeChildrenFlag(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Workflow Include Children Flag";
		public static ID ItemTemplateId => new ID("{7E45FF8A-7E5B-4B39-ABD5-4FC201A950BC}");
		
		public static implicit operator WorkflowIncludeChildrenFlag(Item item) => item != null ? new WorkflowIncludeChildrenFlag(item) : null;
		public static implicit operator Item(WorkflowIncludeChildrenFlag customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}



