using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Sandbox.Foundation.ORM.Models.Manual.Sxa
{
    public interface ISxaEnum
    {
        TextField Value { get; }
    }

    public class SxaEnum : CustomItem, ISxaEnum
    {
        public SxaEnum(Item innerItem)
            : base(innerItem)
        {
        }
        public static string TemplateName => "Enum";
        public static ID ItemTemplateId => new ID("{D2923FEE-DA4E-49BE-830C-E27764DFA269}");

        public TextField Value => new TextField(InnerItem.Fields[FieldConstants.Value.Id]);
        
        public static implicit operator SxaEnum(Item item) => item != null ? new SxaEnum(item) : null;
        public static implicit operator Item(SxaEnum customItem) => customItem?.InnerItem;
        
        public struct FieldConstants
        {
            public struct Value
            {
                public const string FieldName = "Value";
                public static readonly ID Id = new ID("{F917C951-1F75-4D62-B14A-BC6888D7EECA}");
            }
        }
    }
}
