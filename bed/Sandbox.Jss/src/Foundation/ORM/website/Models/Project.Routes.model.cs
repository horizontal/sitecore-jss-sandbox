// ReSharper Disable all
using global::Sitecore.Data;
using global::Sitecore.Data.Fields;
using global::Sitecore.Data.Items;
using global::System.CodeDom.Compiler;

namespace Sandbox.Foundation.Orm.Models.Project.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IGenericPageItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes.IBasePageItem
	{
		ImageField PageFeaturedImageField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class GenericPage : CustomItem, IGenericPageItem
	{
		public GenericPage(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Generic Page";
		public static ID ItemTemplateId => new ID("{2002B16B-4B22-521A-8078-1C28ED81DE29}");
		
		public ImageField PageFeaturedImageField => new ImageField(InnerItem.Fields[FieldConstants.PageFeaturedImage.Id]);
		public CheckboxField ExcludeFromNavigationField => new CheckboxField(InnerItem.Fields[FieldConstants.ExcludeFromNavigation.Id]);
		public DateField LastUpdatedField => new DateField(InnerItem.Fields[FieldConstants.LastUpdated.Id]);
		public TextField MetaDescriptionField => new TextField(InnerItem.Fields[FieldConstants.MetaDescription.Id]);
		public ImageField MetaImageField => new ImageField(InnerItem.Fields[FieldConstants.MetaImage.Id]);
		public ReferenceField PageThemeField => new ReferenceField(InnerItem.Fields[FieldConstants.PageTheme.Id]);
		public CheckboxField SuppressFloatingCtaField => new CheckboxField(InnerItem.Fields[FieldConstants.SuppressFloatingCta.Id]);
		public TextField PageDescriptionField => new TextField(InnerItem.Fields[FieldConstants.PageDescription.Id]);
		public TextField PageTitleField => new TextField(InnerItem.Fields[FieldConstants.PageTitle.Id]);
		public static implicit operator GenericPage(Item item) => item != null ? new GenericPage(item) : null;
		public static implicit operator Item(GenericPage customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct PageFeaturedImage
            {
		        public const string FieldName = "pageFeaturedImage";
		        public static readonly ID Id = new ID("{2AEEDB6A-810D-5CAD-938C-E97BE29A6963}");
            }
            public struct ExcludeFromNavigation
            {
		        public const string FieldName = "excludeFromNavigation";
		        public static readonly ID Id = new ID("{22A4D090-D55C-4DCE-9BB9-7D9ED3D78519}");
            }
            public struct LastUpdated
            {
		        public const string FieldName = "lastUpdated";
		        public static readonly ID Id = new ID("{95026A67-37B4-4D88-88E3-27BA24F84A3E}");
            }
            public struct MetaDescription
            {
		        public const string FieldName = "metaDescription";
		        public static readonly ID Id = new ID("{5E6D77BC-9211-54A4-B2F1-8F69285FC51C}");
            }
            public struct MetaImage
            {
		        public const string FieldName = "metaImage";
		        public static readonly ID Id = new ID("{E73A482A-EE8E-432F-963A-65DAB0617C82}");
            }
            public struct PageTheme
            {
		        public const string FieldName = "pageTheme";
		        public static readonly ID Id = new ID("{D0E54558-7232-5B88-86C8-F615641E8B53}");
            }
            public struct SuppressFloatingCta
            {
		        public const string FieldName = "suppressFloatingCta";
		        public static readonly ID Id = new ID("{A4439BBB-8C0E-4B01-87EE-F3179835CB89}");
            }
            public struct PageDescription
            {
		        public const string FieldName = "pageDescription";
		        public static readonly ID Id = new ID("{4FE8C36B-0728-4973-9207-F4DCE5441F15}");
            }
            public struct PageTitle
            {
		        public const string FieldName = "pageTitle";
		        public static readonly ID Id = new ID("{EDC77533-31EC-5C72-A307-09CD06E8D153}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Project.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IHomePageItem : global::Sandbox.Foundation.Orm.Models.Project.Routes.IGenericPageItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class HomePage : CustomItem, IHomePageItem
	{
		public HomePage(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Home Page";
		public static ID ItemTemplateId => new ID("{8DE1FA3B-0D8E-4605-A261-F7DCD9FF27A2}");
		
		public ImageField PageFeaturedImageField => new ImageField(InnerItem.Fields[FieldConstants.PageFeaturedImage.Id]);
		public CheckboxField ExcludeFromNavigationField => new CheckboxField(InnerItem.Fields[FieldConstants.ExcludeFromNavigation.Id]);
		public DateField LastUpdatedField => new DateField(InnerItem.Fields[FieldConstants.LastUpdated.Id]);
		public TextField MetaDescriptionField => new TextField(InnerItem.Fields[FieldConstants.MetaDescription.Id]);
		public ImageField MetaImageField => new ImageField(InnerItem.Fields[FieldConstants.MetaImage.Id]);
		public ReferenceField PageThemeField => new ReferenceField(InnerItem.Fields[FieldConstants.PageTheme.Id]);
		public CheckboxField SuppressFloatingCtaField => new CheckboxField(InnerItem.Fields[FieldConstants.SuppressFloatingCta.Id]);
		public TextField PageDescriptionField => new TextField(InnerItem.Fields[FieldConstants.PageDescription.Id]);
		public TextField PageTitleField => new TextField(InnerItem.Fields[FieldConstants.PageTitle.Id]);
		public static implicit operator HomePage(Item item) => item != null ? new HomePage(item) : null;
		public static implicit operator Item(HomePage customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct PageFeaturedImage
            {
		        public const string FieldName = "pageFeaturedImage";
		        public static readonly ID Id = new ID("{2AEEDB6A-810D-5CAD-938C-E97BE29A6963}");
            }
            public struct ExcludeFromNavigation
            {
		        public const string FieldName = "excludeFromNavigation";
		        public static readonly ID Id = new ID("{22A4D090-D55C-4DCE-9BB9-7D9ED3D78519}");
            }
            public struct LastUpdated
            {
		        public const string FieldName = "lastUpdated";
		        public static readonly ID Id = new ID("{95026A67-37B4-4D88-88E3-27BA24F84A3E}");
            }
            public struct MetaDescription
            {
		        public const string FieldName = "metaDescription";
		        public static readonly ID Id = new ID("{5E6D77BC-9211-54A4-B2F1-8F69285FC51C}");
            }
            public struct MetaImage
            {
		        public const string FieldName = "metaImage";
		        public static readonly ID Id = new ID("{E73A482A-EE8E-432F-963A-65DAB0617C82}");
            }
            public struct PageTheme
            {
		        public const string FieldName = "pageTheme";
		        public static readonly ID Id = new ID("{D0E54558-7232-5B88-86C8-F615641E8B53}");
            }
            public struct SuppressFloatingCta
            {
		        public const string FieldName = "suppressFloatingCta";
		        public static readonly ID Id = new ID("{A4439BBB-8C0E-4B01-87EE-F3179835CB89}");
            }
            public struct PageDescription
            {
		        public const string FieldName = "pageDescription";
		        public static readonly ID Id = new ID("{4FE8C36B-0728-4973-9207-F4DCE5441F15}");
            }
            public struct PageTitle
            {
		        public const string FieldName = "pageTitle";
		        public static readonly ID Id = new ID("{EDC77533-31EC-5C72-A307-09CD06E8D153}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Project.Routes
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IRedirectItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow.IBaseWorkflowItem
	{
		LinkField RedirectUrlField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class Redirect : CustomItem, IRedirectItem
	{
		public Redirect(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Redirect";
		public static ID ItemTemplateId => new ID("{39474385-54BB-4F44-BCBB-2B2EA1DF91C6}");
		
		public LinkField RedirectUrlField => new LinkField(InnerItem.Fields[FieldConstants.RedirectUrl.Id]);
		public static implicit operator Redirect(Item item) => item != null ? new Redirect(item) : null;
		public static implicit operator Item(Redirect customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct RedirectUrl
            {
		        public const string FieldName = "redirectUrl";
		        public static readonly ID Id = new ID("{41944E83-3B84-49F6-A43D-DE975B37D2FE}");
            }
            
		}
	}
}



