// ReSharper Disable all
using global::Sitecore.Data;
using global::Sitecore.Data.Fields;
using global::Sitecore.Data.Items;
using global::System.CodeDom.Compiler;

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Clients
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IClientItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Data.IBaseDataItem
	{
		MultilistField BusinessVerticalsField { get; }
		ImageField ClientLogoField { get; }
		TextField ClientNameField { get; }
		MultilistField PlatformsField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class Client : CustomItem, IClientItem
	{
		public Client(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Client";
		public static ID ItemTemplateId => new ID("{C3EF0B8E-8DDA-4B19-A7EE-85C7F65AD2FB}");
		
		public MultilistField BusinessVerticalsField => new MultilistField(InnerItem.Fields[FieldConstants.BusinessVerticals.Id]);
		public ImageField ClientLogoField => new ImageField(InnerItem.Fields[FieldConstants.ClientLogo.Id]);
		public TextField ClientNameField => new TextField(InnerItem.Fields[FieldConstants.ClientName.Id]);
		public MultilistField PlatformsField => new MultilistField(InnerItem.Fields[FieldConstants.Platforms.Id]);
		public static implicit operator Client(Item item) => item != null ? new Client(item) : null;
		public static implicit operator Item(Client customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct BusinessVerticals
            {
		        public const string FieldName = "businessVerticals";
		        public static readonly ID Id = new ID("{3F02ECF0-EDB2-422B-94E5-60EB7A719056}");
            }
            public struct ClientLogo
            {
		        public const string FieldName = "clientLogo";
		        public static readonly ID Id = new ID("{259D64A5-4A55-4000-871A-2FE50AEFBB6B}");
            }
            public struct ClientName
            {
		        public const string FieldName = "clientName";
		        public static readonly ID Id = new ID("{791FB8BB-5E13-4BB5-A888-FDCC72C2DAC8}");
            }
            public struct Platforms
            {
		        public const string FieldName = "platforms";
		        public static readonly ID Id = new ID("{DC94E9A6-6A22-423F-8F36-9844CFA1CC13}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Clients
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IClientFolderItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class ClientFolder : CustomItem, IClientFolderItem
	{
		public ClientFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Client Folder";
		public static ID ItemTemplateId => new ID("{6CF2D032-6B7C-42D8-A15D-8AC4F55A895F}");
		
		public static implicit operator ClientFolder(Item item) => item != null ? new ClientFolder(item) : null;
		public static implicit operator Item(ClientFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Taxonomy
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface ICustomTagFolderItem
	{
		TextField PluralLabelField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class CustomTagFolder : CustomItem, ICustomTagFolderItem
	{
		public CustomTagFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Custom Tag Folder";
		public static ID ItemTemplateId => new ID("{5032E154-0FFA-4B23-94C6-FC4460673D55}");
		
		public TextField PluralLabelField => new TextField(InnerItem.Fields[FieldConstants.PluralLabel.Id]);
		public static implicit operator CustomTagFolder(Item item) => item != null ? new CustomTagFolder(item) : null;
		public static implicit operator Item(CustomTagFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct PluralLabel
            {
		        public const string FieldName = "pluralLabel";
		        public static readonly ID Id = new ID("{520A4A61-20BC-4F7E-92A8-F55E42BA8E10}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Navigation.Links
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IIconNavigationLinkItem : global::Sandbox.Foundation.Orm.Models.Feature.Data.Navigation.Links.INavigationLinkItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Data.IBaseDataItem
	{
		ImageField NavigationLinkIconField { get; }
		TextField NavigationLinkIconTitleField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class IconNavigationLink : CustomItem, IIconNavigationLinkItem
	{
		public IconNavigationLink(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Icon Navigation Link";
		public static ID ItemTemplateId => new ID("{0C85D684-5740-449C-B72D-3D68D27B99FF}");
		
		public ImageField NavigationLinkIconField => new ImageField(InnerItem.Fields[FieldConstants.NavigationLinkIcon.Id]);
		public TextField NavigationLinkIconTitleField => new TextField(InnerItem.Fields[FieldConstants.NavigationLinkIconTitle.Id]);
		public LinkField NavigationLinkUrlField => new LinkField(InnerItem.Fields[FieldConstants.NavigationLinkUrl.Id]);
		public static implicit operator IconNavigationLink(Item item) => item != null ? new IconNavigationLink(item) : null;
		public static implicit operator Item(IconNavigationLink customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct NavigationLinkIcon
            {
		        public const string FieldName = "navigationLinkIcon";
		        public static readonly ID Id = new ID("{8C01B5AE-C563-495C-86D0-9BD828072E74}");
            }
            public struct NavigationLinkIconTitle
            {
		        public const string FieldName = "navigationLinkIconTitle";
		        public static readonly ID Id = new ID("{B0F5031D-7C49-4906-AB1D-1F6813801961}");
            }
            public struct NavigationLinkUrl
            {
		        public const string FieldName = "navigationLinkUrl";
		        public static readonly ID Id = new ID("{BC6E386C-4397-4370-9663-40F6ACB01029}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Navigation.Links
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IIconNavigationLinkFolderItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class IconNavigationLinkFolder : CustomItem, IIconNavigationLinkFolderItem
	{
		public IconNavigationLinkFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Icon Navigation Link Folder";
		public static ID ItemTemplateId => new ID("{49186BE2-4210-4ED0-88C8-3CC49830451E}");
		
		public static implicit operator IconNavigationLinkFolder(Item item) => item != null ? new IconNavigationLinkFolder(item) : null;
		public static implicit operator Item(IconNavigationLinkFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Navigation.Links
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface INavigationLinkItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Data.IBaseDataItem
	{
		LinkField NavigationLinkUrlField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class NavigationLink : CustomItem, INavigationLinkItem
	{
		public NavigationLink(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Navigation Link";
		public static ID ItemTemplateId => new ID("{B2920BBE-E8B7-4387-A7DD-7A78EA4E45C5}");
		
		public LinkField NavigationLinkUrlField => new LinkField(InnerItem.Fields[FieldConstants.NavigationLinkUrl.Id]);
		public static implicit operator NavigationLink(Item item) => item != null ? new NavigationLink(item) : null;
		public static implicit operator Item(NavigationLink customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct NavigationLinkUrl
            {
		        public const string FieldName = "navigationLinkUrl";
		        public static readonly ID Id = new ID("{BC6E386C-4397-4370-9663-40F6ACB01029}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Data.Navigation.Links
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface INavigationLinkFolderItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class NavigationLinkFolder : CustomItem, INavigationLinkFolderItem
	{
		public NavigationLinkFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Navigation Link Folder";
		public static ID ItemTemplateId => new ID("{D9C889E9-848C-470F-89E1-FB556446A6FA}");
		
		public static implicit operator NavigationLinkFolder(Item item) => item != null ? new NavigationLinkFolder(item) : null;
		public static implicit operator Item(NavigationLinkFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}



