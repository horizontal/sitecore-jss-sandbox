// ReSharper Disable all
using global::Sitecore.Data;
using global::Sitecore.Data.Fields;
using global::Sitecore.Data.Items;
using global::System.CodeDom.Compiler;

namespace Sandbox.Foundation.Orm.Models.Feature.Components.General.Accordion
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IAccordionComponentItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow.IWorkflowIncludeChildrenFlagItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class AccordionComponent : CustomItem, IAccordionComponentItem
	{
		public AccordionComponent(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Accordion Component";
		public static ID ItemTemplateId => new ID("{CD9A6DCE-8732-4F54-8228-DC0778170E62}");
		
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator AccordionComponent(Item item) => item != null ? new AccordionComponent(item) : null;
		public static implicit operator Item(AccordionComponent customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.General.Accordion
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IAccordionEntryItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		TextField AccordionEntryBodyField { get; }
		TextField AccordionEntryTitleField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class AccordionEntry : CustomItem, IAccordionEntryItem
	{
		public AccordionEntry(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Accordion Entry";
		public static ID ItemTemplateId => new ID("{59FE022A-CF0A-47AF-9C57-45307475FB32}");
		
		public TextField AccordionEntryBodyField => new TextField(InnerItem.Fields[FieldConstants.AccordionEntryBody.Id]);
		public TextField AccordionEntryTitleField => new TextField(InnerItem.Fields[FieldConstants.AccordionEntryTitle.Id]);
		public static implicit operator AccordionEntry(Item item) => item != null ? new AccordionEntry(item) : null;
		public static implicit operator Item(AccordionEntry customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct AccordionEntryBody
            {
		        public const string FieldName = "accordionEntryBody";
		        public static readonly ID Id = new ID("{B4F5E767-8538-4FBC-A5E4-828CB9C244AF}");
            }
            public struct AccordionEntryTitle
            {
		        public const string FieldName = "accordionEntryTitle";
		        public static readonly ID Id = new ID("{3E41FA1D-32C0-4DC7-BE08-FCB3396D63F2}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Listing.Clients
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IClientsListingItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Filter.IBaseFilterableItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class ClientsListing : CustomItem, IClientsListingItem
	{
		public ClientsListing(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Clients Listing";
		public static ID ItemTemplateId => new ID("{E14337EB-9B9D-4362-90DA-17A2D90CAF10}");
		
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public MultilistField AuthorFilterField => new MultilistField(InnerItem.Fields[FieldConstants.AuthorFilter.Id]);
		public MultilistField UserFilterField => new MultilistField(InnerItem.Fields[FieldConstants.UserFilter.Id]);
		public static implicit operator ClientsListing(Item item) => item != null ? new ClientsListing(item) : null;
		public static implicit operator Item(ClientsListing customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            public struct AuthorFilter
            {
		        public const string FieldName = "authorFilter";
		        public static readonly ID Id = new ID("{BE7A1B84-D85F-4D7C-8A8C-687ADC063151}");
            }
            public struct UserFilter
            {
		        public const string FieldName = "userFilter";
		        public static readonly ID Id = new ID("{807BEA77-C3AD-44DC-8847-8A4B07F12487}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.General.ContentBlock
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IContentBlockFolderItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class ContentBlockFolder : CustomItem, IContentBlockFolderItem
	{
		public ContentBlockFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Content Block Folder";
		public static ID ItemTemplateId => new ID("{F5DC45B7-4B85-4090-9FC7-0AB37F25C2FC}");
		
		public static implicit operator ContentBlockFolder(Item item) => item != null ? new ContentBlockFolder(item) : null;
		public static implicit operator Item(ContentBlockFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.General.ContentBlock
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IContentBlockItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseStyleableHeadingWithCtaItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		TextField ContentField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class ContentBlock : CustomItem, IContentBlockItem
	{
		public ContentBlock(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "ContentBlock";
		public static ID ItemTemplateId => new ID("{F0FF41EB-EB09-5AF7-907A-279A823AECD4}");
		
		public TextField ContentField => new TextField(InnerItem.Fields[FieldConstants.Content.Id]);
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public CheckboxField HeadingBrandingField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingBranding.Id]);
		public ReferenceField HeadingSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingSize.Id]);
		public static implicit operator ContentBlock(Item item) => item != null ? new ContentBlock(item) : null;
		public static implicit operator Item(ContentBlock customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Content
            {
		        public const string FieldName = "content";
		        public static readonly ID Id = new ID("{17E5002B-EAC7-5DB5-B6B0-4170F1198A10}");
            }
            public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            public struct HeadingBranding
            {
		        public const string FieldName = "headingBranding";
		        public static readonly ID Id = new ID("{C6918A4E-89EC-449D-8F4D-9DC689D974AC}");
            }
            public struct HeadingSize
            {
		        public const string FieldName = "headingSize";
		        public static readonly ID Id = new ID("{7E5C33BC-FC23-4A36-8340-F4C53C469F7B}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Site
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IFooterItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		TextField FooterBannerField { get; }
		TextField FooterDisclaimerField { get; }
		LinkField FooterFloatingCtaField { get; }
		ImageField FooterLogoField { get; }
		LinkField FooterLogoLinkField { get; }
		TextField FooterLogoLinkTitleField { get; }
		ReferenceField FooterMainNavLinksField { get; }
		ReferenceField FooterSocialItemsField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class Footer : CustomItem, IFooterItem
	{
		public Footer(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Footer";
		public static ID ItemTemplateId => new ID("{4ED9895D-FDCB-5B9D-8E39-8F86DCCF6B86}");
		
		public TextField FooterBannerField => new TextField(InnerItem.Fields[FieldConstants.FooterBanner.Id]);
		public TextField FooterDisclaimerField => new TextField(InnerItem.Fields[FieldConstants.FooterDisclaimer.Id]);
		public LinkField FooterFloatingCtaField => new LinkField(InnerItem.Fields[FieldConstants.FooterFloatingCta.Id]);
		public ImageField FooterLogoField => new ImageField(InnerItem.Fields[FieldConstants.FooterLogo.Id]);
		public LinkField FooterLogoLinkField => new LinkField(InnerItem.Fields[FieldConstants.FooterLogoLink.Id]);
		public TextField FooterLogoLinkTitleField => new TextField(InnerItem.Fields[FieldConstants.FooterLogoLinkTitle.Id]);
		public ReferenceField FooterMainNavLinksField => new ReferenceField(InnerItem.Fields[FieldConstants.FooterMainNavLinks.Id]);
		public ReferenceField FooterSocialItemsField => new ReferenceField(InnerItem.Fields[FieldConstants.FooterSocialItems.Id]);
		public static implicit operator Footer(Item item) => item != null ? new Footer(item) : null;
		public static implicit operator Item(Footer customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct FooterBanner
            {
		        public const string FieldName = "footerBanner";
		        public static readonly ID Id = new ID("{C5361CA3-C24B-40B8-ADFF-AA4171B59846}");
            }
            public struct FooterDisclaimer
            {
		        public const string FieldName = "footerDisclaimer";
		        public static readonly ID Id = new ID("{3B7B0D72-8F2F-5648-A513-886D3FBB574B}");
            }
            public struct FooterFloatingCta
            {
		        public const string FieldName = "footerFloatingCta";
		        public static readonly ID Id = new ID("{0A347719-51FF-44A6-9F95-1A1EA4761640}");
            }
            public struct FooterLogo
            {
		        public const string FieldName = "footerLogo";
		        public static readonly ID Id = new ID("{CC6BAC7E-6D4E-53F8-97EF-D16B3F74A38B}");
            }
            public struct FooterLogoLink
            {
		        public const string FieldName = "footerLogoLink";
		        public static readonly ID Id = new ID("{1A722E41-9C4A-5D77-BB9E-82321E73FFB7}");
            }
            public struct FooterLogoLinkTitle
            {
		        public const string FieldName = "footerLogoLinkTitle";
		        public static readonly ID Id = new ID("{54E04CE6-44D8-5A83-813F-52023C23F3F0}");
            }
            public struct FooterMainNavLinks
            {
		        public const string FieldName = "footerMainNavLinks";
		        public static readonly ID Id = new ID("{8D8D0217-CCFB-4317-97AA-F69FF9B34D69}");
            }
            public struct FooterSocialItems
            {
		        public const string FieldName = "footerSocialItems";
		        public static readonly ID Id = new ID("{CADBF98B-6F73-5CF1-B8D8-63603086DC34}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Site
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IHeaderItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		LinkField HeaderLogoLinkField { get; }
		TextField HeaderLogoLinkTitleField { get; }
		ReferenceField HeaderMainNavLinksField { get; }
		ReferenceField HeaderSecondaryNavLinksField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class Header : CustomItem, IHeaderItem
	{
		public Header(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Header";
		public static ID ItemTemplateId => new ID("{92969004-BA7D-5294-B975-599E32AC3C2B}");
		
		public LinkField HeaderLogoLinkField => new LinkField(InnerItem.Fields[FieldConstants.HeaderLogoLink.Id]);
		public TextField HeaderLogoLinkTitleField => new TextField(InnerItem.Fields[FieldConstants.HeaderLogoLinkTitle.Id]);
		public ReferenceField HeaderMainNavLinksField => new ReferenceField(InnerItem.Fields[FieldConstants.HeaderMainNavLinks.Id]);
		public ReferenceField HeaderSecondaryNavLinksField => new ReferenceField(InnerItem.Fields[FieldConstants.HeaderSecondaryNavLinks.Id]);
		public static implicit operator Header(Item item) => item != null ? new Header(item) : null;
		public static implicit operator Item(Header customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeaderLogoLink
            {
		        public const string FieldName = "headerLogoLink";
		        public static readonly ID Id = new ID("{3FB13BCE-FC31-5EA5-9FA2-EE6901DE2FF5}");
            }
            public struct HeaderLogoLinkTitle
            {
		        public const string FieldName = "headerLogoLinkTitle";
		        public static readonly ID Id = new ID("{88C86118-659C-55D6-A499-4BBED835DDC7}");
            }
            public struct HeaderMainNavLinks
            {
		        public const string FieldName = "headerMainNavLinks";
		        public static readonly ID Id = new ID("{55D677BC-20D7-48CF-8DB0-42E2A6B88B4C}");
            }
            public struct HeaderSecondaryNavLinks
            {
		        public const string FieldName = "headerSecondaryNavLinks";
		        public static readonly ID Id = new ID("{AC066CFB-D839-4AD3-B1D0-021AB7D9429C}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Hero
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IHeroStandardItem : global::Sandbox.Foundation.Orm.Models.Feature.Components.Hero.IHeroBaseItem
	{
		ReferenceField HeroImageContainerSizeField { get; }
		ReferenceField HeroImagePlacementField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class HeroStandard : CustomItem, IHeroStandardItem
	{
		public HeroStandard(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Hero Standard";
		public static ID ItemTemplateId => new ID("{6376C4FE-3643-40F4-A3E5-FF9B9622F998}");
		
		public ReferenceField HeroImageContainerSizeField => new ReferenceField(InnerItem.Fields[FieldConstants.HeroImageContainerSize.Id]);
		public ReferenceField HeroImagePlacementField => new ReferenceField(InnerItem.Fields[FieldConstants.HeroImagePlacement.Id]);
		public LinkField HeroCtaField => new LinkField(InnerItem.Fields[FieldConstants.HeroCta.Id]);
		public TextField HeroHeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeroHeadingText.Id]);
		public ImageField HeroImageField => new ImageField(InnerItem.Fields[FieldConstants.HeroImage.Id]);
		public TextField HeroLabelTextField => new TextField(InnerItem.Fields[FieldConstants.HeroLabelText.Id]);
		public static implicit operator HeroStandard(Item item) => item != null ? new HeroStandard(item) : null;
		public static implicit operator Item(HeroStandard customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeroImageContainerSize
            {
		        public const string FieldName = "heroImageContainerSize";
		        public static readonly ID Id = new ID("{DE821047-73F5-4C8A-AF85-70A5909071CD}");
            }
            public struct HeroImagePlacement
            {
		        public const string FieldName = "heroImagePlacement";
		        public static readonly ID Id = new ID("{0F4120F6-918C-43FB-A091-5655A6ED245F}");
            }
            public struct HeroCta
            {
		        public const string FieldName = "heroCta";
		        public static readonly ID Id = new ID("{0BB075B5-5FA8-47D5-8458-4EAECC40E5E0}");
            }
            public struct HeroHeadingText
            {
		        public const string FieldName = "heroHeadingText";
		        public static readonly ID Id = new ID("{87D185EF-75E1-416D-A500-2C77228C5328}");
            }
            public struct HeroImage
            {
		        public const string FieldName = "heroImage";
		        public static readonly ID Id = new ID("{9629DB62-B583-418F-AF00-A76FBCA547CF}");
            }
            public struct HeroLabelText
            {
		        public const string FieldName = "heroLabelText";
		        public static readonly ID Id = new ID("{F1A332B2-113A-4DAF-8128-ABFA599CAFD7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Layout
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IPageSectionItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		ReferenceField SectionThemeField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class PageSection : CustomItem, IPageSectionItem
	{
		public PageSection(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Page Section";
		public static ID ItemTemplateId => new ID("{0552E73E-2577-4F6F-9667-E143F353B630}");
		
		public ReferenceField SectionThemeField => new ReferenceField(InnerItem.Fields[FieldConstants.SectionTheme.Id]);
		public static implicit operator PageSection(Item item) => item != null ? new PageSection(item) : null;
		public static implicit operator Item(PageSection customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct SectionTheme
            {
		        public const string FieldName = "sectionTheme";
		        public static readonly ID Id = new ID("{6E4F41F7-ED40-43AD-B3CA-C27FA4BB5834}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Promo.PromoFeature
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IPromoFeatureItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingWithDescriptionAndCtaItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		ImageField ImageField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class PromoFeature : CustomItem, IPromoFeatureItem
	{
		public PromoFeature(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Promo Feature";
		public static ID ItemTemplateId => new ID("{0C1D3FE8-6162-4A7A-9014-0EA0F454FAC4}");
		
		public ImageField ImageField => new ImageField(InnerItem.Fields[FieldConstants.Image.Id]);
		public TextField DescriptionField => new TextField(InnerItem.Fields[FieldConstants.Description.Id]);
		public LinkField CtaField => new LinkField(InnerItem.Fields[FieldConstants.Cta.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator PromoFeature(Item item) => item != null ? new PromoFeature(item) : null;
		public static implicit operator Item(PromoFeature customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Image
            {
		        public const string FieldName = "image";
		        public static readonly ID Id = new ID("{6AB903E4-66E1-4140-8F76-872228A6075F}");
            }
            public struct Description
            {
		        public const string FieldName = "description";
		        public static readonly ID Id = new ID("{0F66F48B-00DD-4FB8-866F-433B08271884}");
            }
            public struct Cta
            {
		        public const string FieldName = "cta";
		        public static readonly ID Id = new ID("{666BA83A-5578-4604-A5D5-9CB986DCD516}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Promo.PromoFeature
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IPromoFeatureFolderItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class PromoFeatureFolder : CustomItem, IPromoFeatureFolderItem
	{
		public PromoFeatureFolder(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Promo Feature Folder";
		public static ID ItemTemplateId => new ID("{32B782AF-6241-466A-99E0-4E7718565D0C}");
		
		public static implicit operator PromoFeatureFolder(Item item) => item != null ? new PromoFeatureFolder(item) : null;
		public static implicit operator Item(PromoFeatureFolder customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Layout
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface ISplit2575Item : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseHeadingWithDescriptionItem, global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class Split2575 : CustomItem, ISplit2575Item
	{
		public Split2575(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "Split 25-75";
		public static ID ItemTemplateId => new ID("{EEE24933-25CC-4DA2-BC38-7826E82ACF26}");
		
		public TextField DescriptionField => new TextField(InnerItem.Fields[FieldConstants.Description.Id]);
		public ReferenceField HeadingLevelField => new ReferenceField(InnerItem.Fields[FieldConstants.HeadingLevel.Id]);
		public CheckboxField HeadingScreenReaderOnlyField => new CheckboxField(InnerItem.Fields[FieldConstants.HeadingScreenReaderOnly.Id]);
		public TextField HeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeadingText.Id]);
		public static implicit operator Split2575(Item item) => item != null ? new Split2575(item) : null;
		public static implicit operator Item(Split2575 customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct Description
            {
		        public const string FieldName = "description";
		        public static readonly ID Id = new ID("{0F66F48B-00DD-4FB8-866F-433B08271884}");
            }
            public struct HeadingLevel
            {
		        public const string FieldName = "headingLevel";
		        public static readonly ID Id = new ID("{EE4F8DAC-E02D-4E58-BFCA-86886644F94A}");
            }
            public struct HeadingScreenReaderOnly
            {
		        public const string FieldName = "headingScreenReaderOnly";
		        public static readonly ID Id = new ID("{76DA85C5-10AC-4B4F-A10C-AC199BDE666F}");
            }
            public struct HeadingText
            {
		        public const string FieldName = "headingText";
		        public static readonly ID Id = new ID("{C0E6A8FA-21C9-4B18-9D42-C0E10E3750C7}");
            }
            
		}
	}
}

namespace Sandbox.Foundation.Orm.Models.Feature.Components.Hero
{
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public interface IHeroBaseItem : global::Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Components.IBaseComponentItem
	{
		LinkField HeroCtaField { get; }
		TextField HeroHeadingTextField { get; }
		ImageField HeroImageField { get; }
		TextField HeroLabelTextField { get; }
	}
	[GeneratedCode("Leprechaun", "2.0.0.0")]
	public class HeroBase : CustomItem, IHeroBaseItem
	{
		public HeroBase(Item innerItem)
			:base(innerItem)
		{
		}
		public static string TemplateName => "_Hero Base";
		public static ID ItemTemplateId => new ID("{DF141372-CC20-45BC-A5B7-2B6E6C58A257}");
		
		public LinkField HeroCtaField => new LinkField(InnerItem.Fields[FieldConstants.HeroCta.Id]);
		public TextField HeroHeadingTextField => new TextField(InnerItem.Fields[FieldConstants.HeroHeadingText.Id]);
		public ImageField HeroImageField => new ImageField(InnerItem.Fields[FieldConstants.HeroImage.Id]);
		public TextField HeroLabelTextField => new TextField(InnerItem.Fields[FieldConstants.HeroLabelText.Id]);
		public static implicit operator HeroBase(Item item) => item != null ? new HeroBase(item) : null;
		public static implicit operator Item(HeroBase customItem) => customItem?.InnerItem;
		public struct FieldConstants
		{
			public struct HeroCta
            {
		        public const string FieldName = "heroCta";
		        public static readonly ID Id = new ID("{0BB075B5-5FA8-47D5-8458-4EAECC40E5E0}");
            }
            public struct HeroHeadingText
            {
		        public const string FieldName = "heroHeadingText";
		        public static readonly ID Id = new ID("{87D185EF-75E1-416D-A500-2C77228C5328}");
            }
            public struct HeroImage
            {
		        public const string FieldName = "heroImage";
		        public static readonly ID Id = new ID("{9629DB62-B583-418F-AF00-A76FBCA547CF}");
            }
            public struct HeroLabelText
            {
		        public const string FieldName = "heroLabelText";
		        public static readonly ID Id = new ID("{F1A332B2-113A-4DAF-8128-ABFA599CAFD7}");
            }
            
		}
	}
}



