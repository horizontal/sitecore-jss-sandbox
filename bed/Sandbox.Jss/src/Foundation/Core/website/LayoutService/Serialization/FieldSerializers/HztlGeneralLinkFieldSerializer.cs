using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.LayoutService.Serialization;
using Sitecore.LayoutService.Serialization.FieldSerializers;

namespace Sandbox.Foundation.Core.LayoutService.Serialization.FieldSerializers
{
    public class HztlGeneralLinkFieldSerializer : GeneralLinkFieldSerializer
    {
        public HztlGeneralLinkFieldSerializer(IFieldRenderer fieldRenderer)
          : base(fieldRenderer)
        {
        }

        protected override IDictionary<string, string> GetLinkProperties(LinkField field)
        {
            var dictionary = base.GetLinkProperties(field);

            var itemDisplayName = field.TargetItem?.DisplayName;

            // Set target display name if there is a target item (and it has a name, which it should)
            if(!string.IsNullOrEmpty(itemDisplayName))
            {
                dictionary["targetDisplayName"] = itemDisplayName;
            }

            // If content author manually set a description, save that to "linkDescriptionText" so we know it was manually set
            if (dictionary.TryGetValue("text", out var manuallySetValue) && !string.IsNullOrEmpty(manuallySetValue))
            {
                dictionary["linkDescriptionText"] = manuallySetValue;
            }
            else
            {
                // Otherwise fall back to the item display name
                dictionary["text"] = itemDisplayName;
            }

            return dictionary;
        }

    }
}
