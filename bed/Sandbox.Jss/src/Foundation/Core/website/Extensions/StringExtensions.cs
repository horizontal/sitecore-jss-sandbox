namespace Sandbox.Foundation.Core.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveQueryParam(this string url, string paramName)
        {

            var split = url.Split('?');

            if (split.Length <= 1)
            {
                return url;
            }

            var query = System.Web.HttpUtility.ParseQueryString(split[1]);

            query.Remove(paramName);

            return split[0] + (query.Count > 0 ? "?" + query : "");

        }
    }
}
