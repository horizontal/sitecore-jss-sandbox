using System.Linq;
using JetBrains.Annotations;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data;
using Sitecore.Globalization;
using Sitecore.Services.GraphQL.Content.GraphTypes.ContentSearch;
using Sitecore.Services.GraphQL.EdgeSchema.GraphTypes.Search.Models;
using Sitecore.Services.GraphQL.EdgeSchema.GraphTypes.Search.Predicates;
using Sitecore.Services.GraphQL.EdgeSchema.Services;
using Sitecore.Services.GraphQL.EdgeSchema.Services.Search;

namespace Sandbox.Foundation.Core.Services.GraphQL
{
    [UsedImplicitly]
    public class HztlSearchService : SearchService, ISearchService
    {
        private readonly IPaginationService _paginationService;

        public HztlSearchService(
          IOperationHandlerFactory operationHandlerFactory,
          IPaginationService paginationService) : base (operationHandlerFactory, paginationService)
        {
            _paginationService = paginationService;
        }

        // Explicitly implement the interface because the base method is not virtual and we cannot override it otherwise
        SearchResults ISearchService.GetResults(
          SearchPredicate predicate,
          string after,
          int first,
          OrderingArgument ordering,
          Database database)
        {
            using (var searchContext = ContentSearchManager.GetIndex(new SitecoreIndexableItem(database.GetRootItem())).CreateSearchContext())
            {
                var searchResults = searchContext.GetQueryable<ContentSearchResult>().Where(ResolvePredicate(predicate));
                var num = _paginationService.ResolveSkip(after);
                if (num > 0)
                    searchResults = searchResults.Skip(num);
                if (first > 0)
                    searchResults = searchResults.Take(first);
                if (ordering == null)
                {
                    searchResults = searchResults.OrderByDescending(item => item.Created);
                }
                else
                {
                    switch (ordering.Direction)
                    {
                        case Ordering.ASC:
                            searchResults = searchResults.OrderBy(item => item[ordering.Name]);
                            break;
                        case Ordering.DESC:
                            searchResults = searchResults.OrderByDescending(item => item[ordering.Name]);
                            break;
                    }
                }
                var sortedResults = searchResults.OrderBy(item => item.ItemId).GetResults();

                // This is the part that is modified.
                // The default implementation gets item by ItemUri which includes version
                // Due to timing delay between Sitecore DB update Solr updates, Solr might have
                // an older version than exists in the Sitecore DB
                // This ensures we get the latest version
                var results = sortedResults.Hits.Select(searchHit =>
                    Factory.GetDatabase(searchHit.Document.DatabaseName).GetItem(searchHit.Document.ItemId,
                        Language.Parse(searchHit.Document.Language)))
                    .ToList();

                return new SearchResults
                {
                    Results = results,
                    Total = sortedResults.TotalSearchResults,
                    PageInfo = _paginationService.GetPageInfo(num, results.Count, sortedResults.TotalSearchResults)
                };
            }
        }
    }
}
