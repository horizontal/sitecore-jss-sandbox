using Sandbox.Foundation.DI.Attributes;
using Sitecore;

namespace Sandbox.Foundation.Core.Services.Wrappers
{
    public interface IDateUtilWrapper
    {
        string IsoNow { get; }
    }

    [ServiceImplementation(typeof(IDateUtilWrapper))]
    public class DateUtilWrapperWrapper : IDateUtilWrapper
    {
        public string IsoNow => DateUtil.IsoNow;
    }
}
