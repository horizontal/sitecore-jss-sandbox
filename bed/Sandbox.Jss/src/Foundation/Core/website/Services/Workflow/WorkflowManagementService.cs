using System.Collections.Generic;
using System.Linq;
using Sandbox.Foundation.DI.Attributes;
using JetBrains.Annotations;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Workflows;

namespace Sandbox.Foundation.Core.Services.Workflow
{
    public interface IWorkflowManagementService
    {
        bool UpdateWorkflow([NotNull] Item item, [NotNull] WorkflowState newWorkflowState);

        [CanBeNull]
        WorkflowState GetWorkflowStateForItem([NotNull] Item item);

        [CanBeNull]
        IWorkflow GetWorkflowOfItem([NotNull] Item item);

        bool PublishThroughWorkflow([NotNull] Item item);
    }

    [ServiceImplementation(typeof(IWorkflowManagementService))]
    public class WorkflowManagementService : IWorkflowManagementService
    {
        public Database Database { get; internal set; }

        public WorkflowManagementService()
        {
            Database = Factory.GetDatabase("master");
        }

        public WorkflowManagementService(Database database)
        {
            Database = database;
        }


        public bool PublishThroughWorkflow(Item item)
        {
            return UpdateWorkflow(item, HztlPublishedState);
        }

        public virtual bool UpdateWorkflow(Item item, WorkflowState newWorkflowState)
        {
            Assert.ArgumentNotNull(newWorkflowState, "The new WorkflowState can not be null");
            Assert.ArgumentNotNull(item, "Item can not be null");

            bool successful = false;

            WorkflowState currentWorkflowState = GetWorkflowStateForItem(item);

            if (currentWorkflowState != newWorkflowState)
            {
                IWorkflow workflow = GetWorkflowOfItem(item);

                if (workflow != null && currentWorkflowState != null)
                {
                    List<WorkflowCommand> applicableWorkflowCommands = workflow.GetCommands(currentWorkflowState.StateID).ToList();

                    foreach (var applicableWorkflowCommand in applicableWorkflowCommands)
                    {
                        Item commandItem = Database.GetItem(applicableWorkflowCommand.CommandID);

                        string nextStateId = commandItem["Next state"];

                        if (nextStateId == newWorkflowState.StateID)
                        {
                            WorkflowResult workflowResult = workflow.Execute(applicableWorkflowCommand.CommandID, item, "", false);
                            successful = workflowResult.Succeeded;
                            break;
                        }
                    }
                }
            }
            else
            {
                successful = true;
            }

            return successful;
        }

        public WorkflowState GetWorkflowStateForItem(Item item)
        {
            var workflow = GetWorkflowOfItem(item);
            return workflow?.GetState(item);
        }

        public IWorkflow GetWorkflowOfItem(Item item)
        {
            Assert.ArgumentNotNull(item, "item != null");
            return Database.WorkflowProvider.GetWorkflow(item);
        }
        
        private WorkflowState HztlPublishedState => HztlWorkflow.GetState(ConstantsCore.Workflow.HztlWorkflowStatePublishedId.ToString());

        private IWorkflow HztlWorkflow => Database.WorkflowProvider.GetWorkflow(ConstantsCore.Workflow.HztlWorkflowId.ToString());
    }
}
