using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;

namespace Sandbox.Foundation.Core
{
    public static class ConstantsCore
    {
        public static class Workflow
        {
            public static readonly ID HztlWorkflowId = ID.Parse("{A8916375-150E-4B41-9FAD-F56C1F331789}");
            public static readonly ID HztlWorkflowStatePublishedId = ID.Parse("{BEA9B186-29E0-4BE2-A30D-C5108DA91907}");
        }
    }
}
