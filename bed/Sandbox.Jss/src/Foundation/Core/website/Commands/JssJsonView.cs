using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Pipelines.HasPresentation;
using Sitecore.Publishing;
using Sitecore.Shell.DeviceSimulation;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Sites;
using Sitecore.Text;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Foundation.Core.Commands
{
    [Serializable]
    public class JssJsonView : Command
    {
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, nameof(context));
            if (context.Items.Length != 1)
            {
                return;
            }

            Item obj = context.Items[0];
            Context.ClientPage.Start(this, "Run", new NameValueCollection()
            {
                ["uri"] = obj.Uri.ToString()
            }); ;
        }

        public override CommandState QueryState(CommandContext context)
        {
            Error.AssertObject(context, nameof(context));
    
            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            if (context.Items[0].Visualization.Layout == null)
            {
                return CommandState.Hidden;
            }

            return base.QueryState(context);
        }

        protected void Run(ClientPipelineArgs args)
        {
            Item obj1 = Database.GetItem(ItemUri.Parse(args.Parameters["uri"]));
            if (obj1 == null)
            {
                SheerResponse.Alert("Item not found.", Array.Empty<string>());
            }
            else
            {
                SheerResponse.CheckModified(false);
                SiteContext previewSiteContext = LinkManager.GetPreviewSiteContext(obj1);
                if (previewSiteContext == null)
                {
                    SheerResponse.Alert(Translate.Text("Site \"{0}\" not found", Settings.Preview.DefaultSite), Array.Empty<string>());
                }
                else
                {
                    var apiKeyId = string.Empty;
                    var apiKeyFolder = obj1.Database.GetItem(new ID("{59B41B5F-E22E-460B-941E-FB64FE8DA8FC}"));
                    if (apiKeyFolder != null)
                    {
                        var apiKeys = apiKeyFolder.GetChildren().ToList();
                        var apiKey = apiKeys.FirstOrDefault();

                        if (apiKey != null)
                        {
                            apiKeyId = apiKey.ID.ToString();
                        }
                    }

                    if (string.IsNullOrEmpty(apiKeyId))
                    {
                        SheerResponse.Alert("No api key found under /sitecore/system/Settings/Services/API Keys.", Array.Empty<string>());
                        return;
                    }
                    
                    UrlString urlString = new UrlString("/sitecore/api/layout/render/jss");
                    urlString["item"] = obj1.ID.ToString();
                    urlString["sc_apikey"] = apiKeyId;

                    DeviceSimulationUtil.DeactivateSimulators();

                    if (UIUtil.IsChrome())
                    {
                        SheerResponse.Eval("setTimeout(function () { window.open('" + urlString + "', '_blank');}, 0);");
                    }
                    else
                    {
                        SheerResponse.Eval("window.open('" + urlString + "', '_blank');");
                    }
                }
            }
        }
    }
}
