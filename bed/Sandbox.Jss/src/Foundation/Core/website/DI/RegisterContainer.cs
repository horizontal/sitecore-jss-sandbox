using Algolia.Search.Clients;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Configuration;
using Sitecore.DependencyInjection;

namespace Sandbox.Foundation.Core.DI
{
    [UsedImplicitly]
    public class RegisterContainer : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISearchClient>(x => new SearchClient(
                Settings.GetSetting("Hztl.Algolia.ApplicationId", "7HFGY5QZCD"),
                Settings.GetSetting("Hztl.Algolia.WriteApiKey", "c40874d8b819927cd0d068f23275d08d")));
        }
    }
}
