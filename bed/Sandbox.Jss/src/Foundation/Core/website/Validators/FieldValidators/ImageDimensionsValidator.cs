using System;
using System.Runtime.Serialization;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using Sitecore.Diagnostics;
using Sitecore.Shell.Applications.ContentEditor;

namespace Sandbox.Foundation.Core.Validators.FieldValidators
{
    [Serializable]
    public class ImageDimensionsValidator : StandardValidator
    {
        public ImageDimensionsValidator()
        {
        }

        public ImageDimensionsValidator(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }

        public override string Name => "Image Dimensions";

        public int? MinWidth => ParseIntParameter("min-width");
        public int? MaxWidth => ParseIntParameter("max-width");
        public int? MinHeight => ParseIntParameter("min-height");
        public int? MaxHeight => ParseIntParameter("max-height");
        public double? MinAspectRatio => ParseDoubleParameter("min-aspect-ratio");
        public double? MaxAspectRatio => ParseDoubleParameter("max-aspect-ratio");
        private int? ParseIntParameter(string parameterName)
        {
            var minWidth = Parameters[parameterName];

            if (int.TryParse(minWidth, out var result))
            {
                return result;
            }

            return null;
        }
        private double? ParseDoubleParameter(string parameterName)
        {
            var minWidth = Parameters[parameterName];

            if (double.TryParse(minWidth, out var result))
            {
                return result;
            }

            return null;
        }

        protected override ValidatorResult Evaluate()
        {
            var itemUri = ItemUri;
            if (itemUri == null)
                return ValidatorResult.Valid;

            var field = GetField();
            if (field == null)
                return ValidatorResult.Valid;
            
            var controlValidationValue = ControlValidationValue;
            if (string.IsNullOrEmpty(controlValidationValue) || string.Compare(controlValidationValue, "<image />", StringComparison.InvariantCulture) == 0)
                return ValidatorResult.Valid;
            
            var attribute = new XmlValue(controlValidationValue, "image").GetAttribute("mediaid");
            if (string.IsNullOrEmpty(attribute))
                return ValidatorResult.Valid;
            
            var database = Factory.GetDatabase(itemUri.DatabaseName);
            Assert.IsNotNull(database, itemUri.DatabaseName);
            MediaItem mediaItem = database.GetItem(attribute);
            if (mediaItem == null)
                return ValidatorResult.Valid;

            int width = MainUtil.GetInt(mediaItem.InnerItem["Width"], 0);
            int height = MainUtil.GetInt(mediaItem.InnerItem["Height"], 0);

            // Prevent divide by zero, just to be safe, even though we should always have a height.
            double aspectRatio = height == 0 ? width : (double) width / height;

            if (MinWidth.HasValue && width < MinWidth)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has a width {width} which is less than the minimum {MinWidth}.";
                return GetFailedResult(ValidatorResult.Warning);
            }
            if (MaxWidth.HasValue && width > MaxWidth)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has a width {width} which is more than the maximum {MaxWidth}.";
                return GetFailedResult(ValidatorResult.Warning);
            }
            if (MinHeight.HasValue && height < MinHeight)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has a height {height} which is less than the minimum {MinHeight}.";
                return GetFailedResult(ValidatorResult.Warning);
            }
            if (MaxHeight.HasValue && height > MaxHeight)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has a height {height} which is more than the maximum {MaxHeight}.";
                return GetFailedResult(ValidatorResult.Warning);
            }
            if (MinAspectRatio.HasValue && aspectRatio < MinAspectRatio)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has an aspect ratio {aspectRatio} ({width}/{height}) which is less than the minimum {MinAspectRatio}.  Image should be wider relative to its height.";
                return GetFailedResult(ValidatorResult.Warning);
            }
            if (MaxAspectRatio.HasValue && aspectRatio > MaxAspectRatio)
            {
                Text = $"The image in the field \"{field.DisplayName}\" has an aspect ratio {aspectRatio} ({width}/{height}) which is more than the minimum {MaxAspectRatio}.  Image should be taller relative to its width.";
                return GetFailedResult(ValidatorResult.Warning);
            }

            return ValidatorResult.Valid;

        }

        protected override ValidatorResult GetMaxValidatorResult() => GetFailedResult(ValidatorResult.Error);
    }
}
