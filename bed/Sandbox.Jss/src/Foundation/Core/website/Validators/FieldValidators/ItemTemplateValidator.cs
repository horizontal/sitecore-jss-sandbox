using System;
using System.Linq;
using System.Runtime.Serialization;
using Sitecore.Common;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Validators;
using Sitecore.Diagnostics;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;

namespace Sandbox.Foundation.Core.Validators.FieldValidators
{
    [Serializable]
    public class ItemTemplateValidator : StandardValidator
    {
        public ItemTemplateValidator()
        {
        }

        public ItemTemplateValidator(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }

        public string Template => Parameters["template"];

        public override string Name => "Item Template";

        protected override ValidatorResult Evaluate()
        {
            var itemUri = ItemUri;
            if (itemUri == null)
                return ValidatorResult.Valid;

            var database = Factory.GetDatabase(itemUri.DatabaseName);
            Assert.IsNotNull(database, itemUri.DatabaseName);

            var field = GetField();

            if (string.IsNullOrEmpty(ControlValidationValue))
                return ValidatorResult.Valid;

            var items = ID.ParseArray(ControlValidationValue).Select(x => database.GetItem(x)).WhereNotNull().ToList();

            if (!items.Any())
                return ValidatorResult.Valid;

            if (Template == null)
                return ValidatorResult.Valid;

            var templates = ID.ParseArray(Template).Select(x => database.GetTemplate(x)).WhereNotNull().ToList();

            if (!templates.Any())
                return ValidatorResult.Valid;

            var invalidItems = items.Where(targetItem =>
                    !templates.Any(x=> targetItem.Template.ID == x.ID)
                    && !templates.Any(x => targetItem.Template.DoesTemplateInheritFrom(x.ID)))
                .ToList();
            if (invalidItems.Any())
            {
                Text = $@"Selected item(s) in the field ""{field.DisplayName}"" must match or inherit from ""{string.Join(", ", templates.Select(x=>x.Name))}"".

Invalid items:
{string.Join(Environment.NewLine + Environment.NewLine, invalidItems.Select(x=>$@"""{x.Name}""  (Full Path: {x.Paths.FullPath})"))}
";
                return GetFailedResult(ValidatorResult.CriticalError);

            }
            foreach (var targetItem in invalidItems)
            {
                Text =
                    $"The item \"{targetItem.Paths.FullPath}\" in the field \"{field.DisplayName}\" has has template {targetItem.Template.FullName} which does not inherit from {string.Join(", ", templates.Select(x => x.FullName))}.";
                return GetFailedResult(ValidatorResult.CriticalError);
            }
            return ValidatorResult.Valid;

        }

        protected override ValidatorResult GetMaxValidatorResult() => GetFailedResult(ValidatorResult.Error);
    }
}
